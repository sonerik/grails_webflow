<%--
  Created by IntelliJ IDEA.
  User: sonerik
  Date: 1/7/15
  Time: 11:11 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
</head>

<body>

    <g:each in="${users}">
        <ul>
            <li>${it}</li>
        </ul>
    </g:each>

</body>
</html>