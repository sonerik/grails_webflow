<%--
  Created by IntelliJ IDEA.
  User: sonerik
  Date: 1/4/15
  Time: 3:26 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
</head>

<body>

    <h2>Choose favorite movie genres</h2>

    <hr />

    <g:if test="${message}">
        <div class="message">${message}</div>
    </g:if>
    <g:hasErrors bean="${command}">
        <div class="errors">
            <g:renderErrors bean="${command}" as="list" />
        </div>
    </g:hasErrors>

    <g:form>
        <div class="btn-group-vertical" data-toggle="buttons" style="width: 100%;">
            <g:each in="${genres}" status="i" var="it">
                <label class="btn btn-default">
                    <input type="checkbox" name="favoriteMovieGenres[${i}]" autocomplete="off" value="${it}" /> ${it}
                </label>
            </g:each>
        </div>

        <div style="margin-top: 24px;">
            <g:link event="back" class="btn btn-default">Back</g:link>
            <g:submitButton event="forward" action="createUser" name="forward" value="Next"
                            class="pull-right btn btn-primary"/>
        </div>
    </g:form>

</body>
</html>