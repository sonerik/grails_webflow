<%--
  Created by IntelliJ IDEA.
  User: sonerik
  Date: 1/4/15
  Time: 2:47 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
</head>

<body>

    <h2>Account data</h2>

    <hr />

    <g:if test="${message}">
        <div class="panel panel-warning">
            <div class="panel-heading">${message}</div>
            <div class="panel-body">
                <g:hasErrors bean="${command}">
                    <g:renderErrors bean="${command}" as="list" />
                </g:hasErrors>
            </div>
        </div>
    </g:if>

    <g:form>
        <div class="form-group ${hasErrors(bean: command, field: 'name', 'has-error')}">
            <label for="name">Name</label>
            <g:textField name="name" class="form-control" id="name" placeholder="Name" value="${fieldValue(bean:command, field:'name')}" />
        </div>
        <div class="form-group ${hasErrors(bean: command, field: 'email', 'has-error')}">
            <label for="email">Email</label>
            <g:textField name="email" datatype="email" class="form-control" id="email" placeholder="Email" value="${fieldValue(bean:command, field:'email')}" />
        </div>
        <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')}">
            <label for="password">Password</label>
            <g:passwordField name="password" datatype="password" class="form-control" id="password" placeholder="Password" value="${fieldValue(bean:command, field:'password')}" />
        </div>

        <div>
            <g:link event="back" class="btn btn-default">Back</g:link>
            <g:submitButton event="forward" action="createUser" name="forward" value="Next"
                            class="pull-right btn btn-primary"/>
        </div>
    </g:form>

    %{--<nav>--}%
        %{--<ul class="pager">--}%
            %{--<li class="previous"><g:link event="back" ><span aria-hidden="true">&larr;</span> Back</g:link></li>--}%
            %{--<li class="next"><g:link event="forward" action="createUser" >Forward <span aria-hidden="true">&rarr;</span></g:link></li>--}%
        %{--</ul>--}%
    %{--</nav>--}%

</body>
</html>