<%--
  Created by IntelliJ IDEA.
  User: sonerik
  Date: 1/4/15
  Time: 8:21 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
</head>

<body>

    <h2>Welcome to my user creation wizard!</h2>

    <p>You will create a new user with some data here.</p>

    <div class="row">
        <g:link name="next" event="forward" class="pull-right btn btn-primary">Let's start!</g:link>
    </div>

</body>
</html>