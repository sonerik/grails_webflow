<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main">
	<title></title>
</head>

<body>

<div class="row">
	<h2 class="pull-left">Users list</h2>
	<g:link action="createUser" class="pull-right btn btn-primary">Create new!</g:link>
</div>

<div class="row">
	<table class="users-table well table table-striped">
		<tr>
			<th>
				ID
			</th>
			<th>
				Name
			</th>
			<th>
				Email
			</th>
			<th>
				Favorite music genres
			</th>
			<th>
				Favorite movie genres
			</th>
		</tr>
	<g:each in="${users}">
		<tr>
			<td>
				${it.id}
			</td>
			<td>
				${it.name}
			</td>
			<td>
				${it.email}
			</td>
			<td>
				<g:each in="${it.favoriteMusicGenres}" var="m">
					<span class="label label-primary">${m}</span>
				</g:each>
			</td>
			<td>
				<g:each in="${it.favoriteMovieGenres}" var="m">
					<span class="label label-primary">${m}</span>
				</g:each>
			</td>
		</tr>
	</g:each>
	</table>
</div>
<script>
	$(".users-table tr").click(function(){
		window.location = "/user/show/"+$("td", this).eq(0).text();
	});
</script>
</body>
</html>