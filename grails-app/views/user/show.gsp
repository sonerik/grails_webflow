<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main">
	<title></title>
</head>

<body>

<h2>User data</h2>

<hr />

<p>Name: ${user.name}</p>
<p>Email: ${user.email}</p>
<p>Password: <button type="button"
					 data-toggle="modal"
					 data-target="#just_kidding"
					 class="btn btn-danger btn-sm" >
	Click here to see!
</button>
</p>

<g:if test="${user.favoriteMusicGenres}">
<div class="panel panel-primary">
	<div class="panel-heading">Favorite music genres:</div>
	<div class="panel-body">
		<g:each in="${user.favoriteMusicGenres}">
			<span class="label label-primary">${it}</span>
		</g:each>
	</div>
</div>
</g:if>

<g:if test="${user.favoriteMovieGenres}">
<div class="panel panel-primary">
	<div class="panel-heading">Favorite movie genres:</div>
	<div class="panel-body">
		<g:each in="${user.favoriteMovieGenres}">
			<span class="label label-primary">${it}</span>
		</g:each>
	</div>
</div>
</g:if>

<div id="just_kidding" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content well">
			You already know it. :P
		</div>
	</div>
</div>

<g:link class="btn btn-primary" action="index" >Show users list</g:link>

</body>
</html>