package groovy_controllers

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def list() {
        [users: User.list()]
    }

    def index = {
        [users: User.list(params)]
    }

    def show(User user) {
        [user: user]
    }

    def create() {
        redirect(action: "createUser")
    }

    def createUserFlow = {
        init {
            action {
                flow.user = new User()
                [user: flow.user]
            }
            on("success").to "displayGreetings"
        }

        displayGreetings {
            on("forward").to "enterPersonalData"
        }

        enterPersonalData {
            on("back").to "displayGreetings"
            on("forward") { BuildPersonalDataCommand command ->
                if (command.hasErrors()) {
                    flash.message = "Validation error"
                    flow.command = command
                    return error()
                }
                bindData(flow.user, command)
                [user: flow.user, genres: [
                        "Alternative rock",
                        "Glam rock",
                        "Hard rock",
                        "Heavy metal",
                        "Alternative metal",
                        "Nu metal",
                        "Black metal",
                        "Viking Metal",
                        "Christian metal",
                        "Death metal",
                        "Metalcore",
                        "Power metal",
                        "Progressive metal",
                        "Djent",
                        "Thrash metal",
                        "Post-hardcore",
                        "Screamo",
                        "Emo"
                ]]
            }.to "chooseMusicGenres"
        }

        chooseMusicGenres {
            on("forward") { BuildFavoriteMusicGenresCommand command ->
                command.favoriteMusicGenres = command.favoriteMusicGenres.findAll()
                if (command.hasErrors()) {
                    flash.message = "Validation error"
                    flow.command = command
                    return error()
                }
                bindData(flow.user, command)
                [user: flow.user, genres: [
                        "Action/Adventure",
                        "Drama",
                        "Romance",
                        "Comedy",
                        "Documentary",
                        "Horror",
                        "Thriller"
                ]]
            }.to "chooseMovieGenres"
            on("back").to "enterPersonalData"
        }

        chooseMovieGenres {
            on("forward") { BuildFavoriteMovieGenresCommand command ->
                command.favoriteMovieGenres = command.favoriteMovieGenres.findAll()
                if (command.hasErrors()) {
                    flash.message = "Validation error"
                    flow.command = command
                    return error()
                }
                bindData(flow.user, command)
                [user: flow.user]
            }.to "saveUser"
        }

        saveUser {
            action {
                if (!flow.user.hasErrors() && flow.user.save()) {
                    return success()
                } else {
                    return error()
                }
            }
            on('success').to('showUser')
            on('error').to('chooseMovieGenres')
        }

        showUser {
            redirect(controller: 'user', action: 'show', params: [id: flow.user.id])
        }

    }

    class BuildPersonalDataCommand implements Serializable {
        String name
        String email
        String password

        static constraints = {
            name size: 3..20, blank: false
            password size: 5..20, blank: false
            email email: true, blank: false, unique: true, validator: {
                if (it in User.list().collect {it.email}) 'user.email.already_exists.message' else true
            }
        }

    }

    class BuildFavoriteMusicGenresCommand implements Serializable {
        List<String> favoriteMusicGenres

        static constraints = {
        }

    }

    class BuildFavoriteMovieGenresCommand implements Serializable {
        List<String> favoriteMovieGenres

        static constraints = {
        }

    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance) {
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
