package groovy_controllers

class User implements Serializable {

    String name
    String email
    String password
    List<String> favoriteMusicGenres
    List<String> favoriteMovieGenres

    transient static hasMany = [
            favoriteMusicGenres: String,
            favoriteMovieGenres: String
    ]

    transient static constraints = {
        name size: 3..20, blank: false
        password size: 5..20, blank: false
        email email: true, blank: false, unique: true
    }
}
